<?php

$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 'file';

$cacheConfigurations = &$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'];

// Uncommenting the two lines below will slow down request times dramatically
//$cacheConfigurations['cache_core']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
//$cacheConfigurations['fluid_template']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';

$cacheConfigurations['cache_hash']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['cache_pages']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['cache_pagesection']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['cache_phpcode']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['cache_rootline']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['extbase_datamapfactory_datamap']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['extbase_object']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['extbase_reflection']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['extbase_typo3dbbackend_queries']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['extbase_typo3dbbackend_tablecolumns']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
$cacheConfigurations['l10n']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';

// News caches
//$cacheConfigurations['cache_news_classes']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';
//$cacheConfigurations['cache_news_category']['backend'] = 'TYPO3\CMS\Core\Cache\Backend\NullBackend';

// Mail
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'mbox';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_mbox_file'] = PATH_site . 'typo3temp/sent-mails.txt';
